A simple java console application that allows to play TicTacToe against your friend or computer.
------------------------------------------------------------------------------------------------
Requirements: Java JDK>=6

To run from console, run below command;
>javac path/to/MainApplication.java
>java path/to/MainApplication

To run from JAR file, run below command;
>java -jar path/to/tictactoe.jar

Thanks!